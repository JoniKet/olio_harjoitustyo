/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import static java.time.Clock.system;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import java.util.HashMap;
import java.util.ArrayList;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.input.MouseEvent;

/**
 *
 * @author Jönnsson
 */
public class MainWindowController implements Initializable {
    
    private Label label;
    @FXML
    private Button createPackgage;
    @FXML
    private WebView web;
    @FXML
    private ComboBox<String> placeList;
    @FXML
    private Button addToMapButton;
    @FXML
    private Button sendButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button removeButton;
    @FXML
    private ComboBox<Package> stuffList;
    @FXML
    private Button Lopeta;
    @FXML
    private Label moneyLabel;
    @FXML
    private Button hireButton1;

    public ComboBox<Package> getStuffList() {
        return stuffList;
    }

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //Ladataan karttawebruutuunn
        web.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        //ladataan paikkakunnat listaan
        try {
            url = new URL("http://smartpost.ee/fi_apt.xml");
        } catch (MalformedURLException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        //url tavara menee content muuttujaan
        String content = "";
        
        //luodaan databuilder, joka on koko ajan sama käynnin ajan
        DataBuilder db = new DataBuilder();
        
        try {
            content = db.urlKäsittely(url);
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            db.WebKäsittely(content, "place");
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        placeList.getItems().addAll(DataBuilder.getPaikkakunnat());
        SCPackage generic1 = new SCPackage(SmartPost.getSmartPostList1().get(65),SmartPost.getSmartPostList1().get(255),Storage.stuff.get(0));
        updatePacketList();
        
        
        
        openHelpWindow("Tervetuloa pelaamaan Timo pakettipeliä. Tehtävänäsi on kerätä timon kukkaroon niin paljon rahaa, että timolla on varaa palkata teekkari.\n"
                + "Kannattaa aloittaa kuskaamalla kaliaa lappeenrantaa, sillä tällä hyödykkeellä on varma kysyntä. Jatkossa kannattaa kuitenkin siirtyä kannattavampaan logistiikkaan!");
      
    }    

    
    
    // Napista avataan paketinluontiikkuna
    @FXML
    private void openPackgageWindow(ActionEvent event) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("Package_Window.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            Stage stage2 = new Stage();
            stage2.setTitle("Paketinluontiikkuna");
            stage2.setScene(new Scene(root1));  
            stage2.show();
        } catch (IOException ex) {
            Logger.getLogger(MainWindowController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML //Lisää kartalle paikkakunnan smartpost automaatit
    private void addToMap(ActionEvent event) {
        String sijainti;
        String blue = "blue";
        DataBuilder db = new DataBuilder();
        for(SmartPost sp2:SmartPost.getSmartPostList1()){
            if(sp2.getPaikkakunta() == null ? placeList.getValue() == null : sp2.getPaikkakunta().equals(placeList.getValue())){
                sijainti = "<"+sp2.getOsoite()+">,<"+sp2.getCode()+"><"+sp2.getPaikkakunta()+">";
                web.getEngine().executeScript("document.goToLocation('"+sijainti+"','"+sp2.getSijainti()+"','"+blue+"')");
            }
        }
        
    }



    @FXML
    private void updatePacketList() {
        stuffList.getItems().clear();
        stuffList.getItems().addAll(Package.getPackageList());
    }

 
    @FXML
    private void drawParcelToMap(ActionEvent event) {
        try{
            Kirjanpito kirjanpito = new Kirjanpito();
            ArrayList<String> coordinates = new ArrayList<>();
            coordinates.add(stuffList.getValue().startBox.getLatitude());
            coordinates.add(stuffList.getValue().startBox.getLongitude());
            coordinates.add(stuffList.getValue().endBox.getLatitude());
            coordinates.add(stuffList.getValue().endBox.getLongitude());
            String sijainti1;
            String sijainti2;
            String blue = "blue";
            sijainti1 = "<"+stuffList.getValue().startBox.getOsoite()+">,<"+stuffList.getValue().startBox.getCode()+"><"+stuffList.getValue().startBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti1+"','"+stuffList.getValue().startBox.getSijainti()+"','"+blue+"')");
            sijainti2 = "<"+stuffList.getValue().endBox.getOsoite()+">,<"+stuffList.getValue().endBox.getCode()+"><"+stuffList.getValue().endBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti2+"','"+stuffList.getValue().endBox.getSijainti()+"','"+blue+"')");
            String red = "red";
            
            addMoney(stuffList.getValue().nopeus,stuffList.getValue().object.weight);
            
            Double distance;
            distance = (Double) web.getEngine().executeScript("document.createPath("+coordinates+",'"+red+"',"+stuffList.getValue().nopeus+")");
            
            try{
                kirjanpito.parcelToKirjanpito(stuffList.getValue().startBox, stuffList.getValue().endBox, distance);
            }catch(Exception e){
                openHelpWindow("Kirjanpitäjä otti hatkat");
            }

            if(!"Kalia".equals(stuffList.getValue().object.nimi)){
                Package.getPackageList().remove(stuffList.getValue());
                updatePacketList();
            }    
            
        }catch(Exception e){
            openHelpWindow("Oletko valinnut paketin?\n ");
        }
    }
    private void addMoney(int nopeus, float paino){
        int baserate = (4-nopeus)*10;
        float lisattava = baserate + paino*1;
        int rounded = (int) lisattava;
        String before = moneyLabel.getText();
        int beforeint = Integer.parseInt(before);
        int afterint = rounded + beforeint;
        moneyLabel.setText(Integer.toString(afterint));
    }

    @FXML
    private void removeShitsFromMap(ActionEvent event) {
        web.getEngine().executeScript("document.deletePaths()");
    }
    
    
    private void openHelpWindow(String text){
        
        try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("INFFO.fxml"));
                Parent root1 = (Parent) fxmlLoader.load();
                Stage stage2 = new Stage();
                stage2.setTitle("Vinkki: päivitä ydinpommi!");
                stage2.setScene(new Scene(root1));
                stage2.show();
                
                INFFOController controller = fxmlLoader.getController();
                controller.getHelpTextBox().setText(text + "\n\n paina 2x q putkeen tekstin lopussa sulkeaksesi ikkunan");
                controller.helpTextBox.autosize();
            } catch (IOException ex) {
                Logger.getLogger(Package_WindowController.class.getName()).log(Level.SEVERE, null, ex);
            }
    }

    @FXML
    private void updateParcel(ActionEvent event) {
        openHelpWindow("Yrität kovasti päivittää pakettia.\n Hoksaat, että eihän pakettia voi päivittää. Intensiivinen käsienpyörittely paketin ympärillä ei tuota tulosta");
    }

    @FXML
    private void quitProgram(ActionEvent event) throws IOException {
        Kirjanpito kp = new Kirjanpito();
        kp.writeReport();
        Stage stage = (Stage) Lopeta.getScene().getWindow(); 
        stage.close();  
    }

    @FXML
    private void hireWorkers(ActionEvent event) throws InterruptedException {
        for(int i = 0; i < 500; i++){
            System.out.println(i);
            Worker worker = new Worker();
            Package scp = worker.doWork();
            Kirjanpito kirjanpito = new Kirjanpito();
            ArrayList<String> coordinates = new ArrayList<>();
            coordinates.add(scp.startBox.getLatitude());
            coordinates.add(scp.startBox.getLongitude());
            coordinates.add(scp.endBox.getLatitude());
            coordinates.add(scp.endBox.getLongitude());
            String sijainti1;
            String sijainti2;
            String blue = "blue";
            sijainti1 = "<"+scp.startBox.getOsoite()+">,<"+scp.startBox.getCode()+"><"+scp.startBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti1+"','"+scp.startBox.getSijainti()+"','"+blue+"')");
            sijainti2 = "<"+scp.endBox.getOsoite()+">,<"+scp.endBox.getCode()+"><"+scp.endBox.getPaikkakunta()+">";
            web.getEngine().executeScript("document.goToLocation('"+sijainti2+"','"+scp.endBox.getSijainti()+"','"+blue+"')");
            String red = "red";

            addMoney(scp.nopeus,scp.object.weight);


            web.getEngine().executeScript("document.createPath("+coordinates+",'"+red+"',"+scp.nopeus+")");


            if(!"Kalia".equals(scp.object.nimi)){
                Package.getPackageList().remove(scp);
                updatePacketList();
            }
        }
    
    }
    private void drawMap(){
}
