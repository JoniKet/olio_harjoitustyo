/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import java.util.Set;

/**
 *
 * @author Jönnsson
 */
public class Kirjanpito {
    static private Kirjanpito kirjanpito = null;
    
    private static int sentParcels;

    private static HashMap<SmartPost, Integer> startSmartBoxes = new HashMap<>();
    private static HashMap<SmartPost, Integer> endSmartBoxes = new HashMap<>();
    
    private static ArrayList<Double> deliveryDistanceList = new ArrayList<>();
    
    
    static public Kirjanpito getInstance() {
        if(kirjanpito == null){
            kirjanpito = new Kirjanpito();
        }
        return kirjanpito;
    }
    public Kirjanpito(){
        
    }
    
    public void parcelToKirjanpito(SmartPost start,SmartPost end,Double distance){
        
        
        System.out.println(startSmartBoxes.size());
        if(startSmartBoxes.containsKey(start)== false){
            startSmartBoxes.put(start, 1);
        }
        else if(startSmartBoxes.containsKey(start) == true){
            int i = startSmartBoxes.get(start);
            int j = i+1;
            startSmartBoxes.replace(start, i, j);
        }
        if(endSmartBoxes.containsKey(end)== false){
            endSmartBoxes.put(end, 1);
        }
        else if(endSmartBoxes.containsKey(end) == true){
            int i = endSmartBoxes.get(end);
            int j = i+1;
            endSmartBoxes.replace(end, i, j);
        }
        deliveryDistanceList.add(distance);
        sentParcels = sentParcels +1;
    }
    
    public void writeReport() throws IOException{
        InputOutput io = new InputOutput();
        
        io.writeText("Lähetetyt paketit yhteensä: "+String.valueOf(sentParcels));

        io.writeText("Lähetettyjä paketteja smartpostista:  "+startSmartBoxes.toString());
        
        io.writeText("Vastaanotettuja paketteja smartpostista:  "+endSmartBoxes.toString());
        
        io.writeText("Lista toimitusmatkoista kilometreinä: "+deliveryDistanceList.toString());
        
    }
    
    
    
    
}
