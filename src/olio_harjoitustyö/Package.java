/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;
import java.util.ArrayList;
/**
 *
 * @author Jönnsson
 */
public abstract class Package {
    SmartPost startBox;
    SmartPost endBox;
    Object object;
    int nopeus;
    
    public static ArrayList<Package> packageList = new ArrayList<>();

    public static ArrayList<Package> getPackageList() {
        return packageList;
    }
    
    public Package(SmartPost startBox1,SmartPost endBox1,Object object1){
        startBox = startBox1;
        endBox = endBox1;
        object = object1;
        packageList.add(this);
    }
    
     @Override
    public String toString(){
       String temp;
       temp = object.getNimi();
       return temp;
   }
}
