/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Jönnsson
 */
public class OLIO_HARJOITUSTYÖ extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        
        InputOutput io = new InputOutput();
        io.emptyFile();
        
        
        Object Kalia = new Object("Kalia", (float) 100.0, (float) 0.01, true);
        Object ydinpommi = new Object("ydinpommi", (float) 100.0,1500,true);
        Object kylterinKyynnel = new Object("Kylterin Kyynel", (float) 1.0, 2, false);
        Object Jarmo_Petteri = new Object("Jarmo_Petteri", (float) 100.0, 500, true);
        Object Veitsi = new Object("Veitsi", (float) 5.0, 5, false);
        Object Hulilupteri = new Object("Hulilupteri", (float) 100.0, 1000, true);
        
        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
        
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
