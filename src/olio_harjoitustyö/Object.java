/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio_harjoitustyö;

/**
 *
 * @author Jönnsson
 */
public class Object {
    String nimi;
    float size;
    float weight;
    boolean breakable;
    
    public Object(String nimi1,float size1, float weight1, boolean breakable1){
        nimi = nimi1;
        size = size1;
        weight = weight1;
        breakable = breakable1;
        Storage.stuff.add(this);
    }

    public String getNimi() {
        return nimi;
    }
    
    @Override
   public String toString(){
       String temp;
       temp = nimi;
       return temp;
   }
}
